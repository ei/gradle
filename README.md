# Gradle Config
* Copy **init.gradle** to your **~/.gradle/** directory
 * wget https://gitlab.rrze.fau.de/ei/gradle/raw/master/init.gradle
* Copy **artifactory.gradle** to your **~/.gradle/** directory
 * wget https://gitlab.rrze.fau.de/ei/gradle/raw/master/artifactory.gradle
 * Replace *MYUSERNAME* with your IdM username
 * Replace *MYKEY* with your artifactory key (details in the artifactory.gradle file)

# Gradle Daemon
* add: *org.gradle.daemon=true* to your **~/.gradle/gradle.properties**

# Plugin Publishing:
* in **build.gradle** add: *apply from:repo_publish_config*

# Maven Publishing:
* in **build.gradle** add: 
  * *apply plugin: 'maven-publish'*
  * *apply from:maven_publish_config*
  * <pre>publishing { publications {
		mavenJava(MavenPublication) {
			from components.java
		    }
	    }
    }</pre>
